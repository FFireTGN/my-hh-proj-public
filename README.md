### Greatings to my potential future employer!

## What's this?

Project for CV for demonstration purpose only. Uses gitlab CI/CD, terraform, ansible and helm to provide sample application through ci/cd pipeline from absolutely nothing to working deployment on self-managed kubernetes cluster in AWS ec2-instances. Shows only the technology knowledge of the author and nothing else :) Can hardly imagine this in production - well, in real world we have at least EKS for such things, but...

## Key features

- provides cluster with container-d CRI, without docker daemon intermediary (should be a little bit faster)
- uses default deny network policy inside cluster for advanced inner network security
- resource quota for memory overflow prevention 
- istio-based network and modified istio-ingress controller with tiny route optimization
- loadbalancer healthchecks and inner pods readyness probes for high-availability
- nodes auto recovery and self-restoring on ec2-failing (for worker nodes only)

but it has also some economical optimizations, not suitable for production:

- force kube-sheduler to shedule pods on master node, which is slow and unsafe 
- creates only one master-node, so isnt as safe and stable as should be
- as far as I know, using istio is never preferable for small clusters

## Tuning

You can safely tune some important values by editing variables.tf:

- amount of nodes in the cluster (this will also affects on amount of pods)
- type of nodes in the cluster (you might want to enlarge limits in resourcequota if do so)
- region

DO NOT edit any other values if you dont know what you are doing :)

You may also edit nodeport value in .gitlab-ci environments section, but anyway that port is not accessable through internet, so there is no meaning to do this.

## Deploying your application

Its supposed, that user would replace dockerfile in site_test_content folder with his own one to run his own deployment.
On the other hand, you can keep dockerfile and replace index.php file to run simple php-based site, my dockerfile will make image with nginx and php-fpm inside and deploy it to the cluster. Nginx configuration is inside a helm-chart in configmap.yaml - you might want to edit it, unset, if you do not need it, or replace with any other configuration for your imagined application.

## Usage

To use this project you must push in my main branch. That's all, gitlab runner do all the other things.
Obviously, you can't commit in my master branch :) Consider this project to be read only mostly. You can also watch how it works here:

https://youtu.be/8lCZ23e3hiw

But, to tell the truth, you still can try to clone it and run it at your own (although, I have no idea, in which case you may need this). 

First, you need to setup your own gitlab shell-runner - install at least AWS cli, docker, terraform, ansible and kubectl to it. This runner should be registered in gitlab as specific runner.

Second, add masked variables AWS_ACCESS_KEY and AWS_SECRET_KEY with your AWS credentials into your gitlab settings>ci/cd>variables section.

Finally, you should also create gitlab-deploy-token with scope "read_registry" to allow helm chart to pull image from repository.

Than you may clone my repo, and if I have not forgotten anything, it might work.


## Structure of the project

.gitlab-ci - the main file, from which you should start observation of the project. In fact, this is the basis of this project, which calls other stacks.

main.tf - terraform infrustructure declaration, I probably should have to split it to logical parts or even modules, but I think this project is not so big and important to do this. Sorry, if I wrong ). this creates autoscaling group with ec2 instancs and loadbalancer.

ansibleclusterinit.yml - an ansible playbook for setuping self-managed cluster upon created infrustructure with kubeadm, uses container-d as CRI and weave.net as CNI.

helmchart/templates - contains my helm-chart for this project. Note, that this chart deploys not only application image, but also contains a plenty of manifests, which configures whole cluster, such as networking, network policies, ingress controller, resourcequote, pod disruption budget, etc.

site_test_content - contains my dockerfile and simple php page.

other_experiments - is folder with files which is not used in this project now/yet, but I am still experementing with them.

userdata.sh is init scrypt for ec2-instances

## Contacts

I have no experience at all in devops or SRE and has never worked with this technologies before, just want to try myself here. 
You may contact me by e-mail: flash-asn@inbox.ru
