variable "setregion" {
  type = string
  default = "eu-central-1"
}

variable "clusternodecount" {
  description = "Amount of nodes"
  type = number
  default = 3
}

variable "instance_type" {
  type = string
  default = "t2.medium" #при изменении - проверить resourcequota
}

variable "volume_size" {
  type = number
  default = 30
}

variable "AWS_ACCESS_KEY" {
  type = string
} 

variable "AWS_SECRET_KEY" {
  type = string
} 

variable "nodeport" {
  type    = number
  default = 32080 #единой точкой переопределения является .gitlab-сi.yaml, не изменять чтобы согласовалось с helm при отсутствии значений из ci
}