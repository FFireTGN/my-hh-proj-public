provider "aws" {
  region = var.setregion
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_KEY
}

terraform {
  backend s3 {
    bucket   = "hh-project"
    key = "terraform/terraform.tfstate"
    region = "eu-central-1"
      }
}

data "aws_availability_zones" "available" {}

data "aws_ami" "l_ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

#sg

resource "aws_security_group" "Worldports" {

  dynamic "ingress" {
    for_each = ["80", "443"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value      
      protocol    = "TCP"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    }  
}


# порты отсюда: https://kubernetes.io/docs/reference/ports-and-protocols/ + порты weave cni

resource "aws_security_group" "Netports" {

  dynamic "ingress" {
    for_each = ["6783", "6443", "2379", "2380", "10250", "10259", "10257", "22"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      
      protocol    = "TCP"
      cidr_blocks = [aws_default_subnet.default_az1.cidr_block, aws_default_subnet.default_az2.cidr_block, aws_default_subnet.default_az3.cidr_block]     
    }
  }

  dynamic "ingress" {
    for_each = ["6783", "6784"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "udp"
      cidr_blocks = [aws_default_subnet.default_az1.cidr_block, aws_default_subnet.default_az2.cidr_block, aws_default_subnet.default_az3.cidr_block]
    }
  }
  
  tags = {
    Name = "k8s and cni internal ports"
  }
}
    

resource "aws_security_group" "IstioPorts" {

    ingress {
    from_port   = 15000
    to_port     = 15021  

    protocol    = "tcp"
    cidr_blocks = [aws_default_subnet.default_az1.cidr_block, aws_default_subnet.default_az2.cidr_block, aws_default_subnet.default_az3.cidr_block]
    }
  
  tags = {
    Name = "Istio"
  }
}


resource "aws_security_group" "NodePort" {

    ingress {
    from_port   = var.nodeport
    to_port     = var.nodeport
    protocol    = "tcp"
    cidr_blocks = [aws_default_subnet.default_az1.cidr_block, aws_default_subnet.default_az2.cidr_block, aws_default_subnet.default_az3.cidr_block]
    }
  
  tags = {
    Name = "Nodeport"
  }
}



#доступ инстансов к s3 для самореанимации

resource "aws_iam_policy" "s3_read_policy" {
  name        = "s3_read_policy"
  policy      = file("s3bucketpolicy.json")
}

resource "aws_iam_role" "s3_access_role" {
  name               = "s3-role"
  assume_role_policy = file("s3assumerolepolicy.json")
}

resource "aws_iam_policy_attachment" "s3policyattachment" {
  name       = "s3policyattachment"
  roles      = [aws_iam_role.s3_access_role.name]
  policy_arn = aws_iam_policy.s3_read_policy.arn
}

resource "aws_iam_instance_profile" "s3_profile" {
  name  = "s3_profile"
  role = aws_iam_role.s3_access_role.name
}


#asg

resource "aws_launch_configuration" "nodes" {
  name_prefix     = "nodes_autolaunchconfig-"
  image_id        = data.aws_ami.l_ubuntu.id
  instance_type   = var.instance_type
  security_groups = [aws_security_group.NodePort.id, aws_security_group.Worldports.id, aws_security_group.Netports.id, aws_security_group.IstioPorts.id]
  key_name = "AWS_3"
  iam_instance_profile = "${aws_iam_instance_profile.s3_profile.name}"
  user_data = file("userdata.sh") #этот скрипт триггернёт гит, чтобы автоматически добавить ноду в кластер, если она будет перезапущена ASG, а не ci/cd процессом
  # на данный момент у меня тут поддерживается самореанимация только worder-нод, т.к. этот кластер с 1м мастером в целях экономии
    
  root_block_device {
    volume_size           = var.volume_size
    volume_type           = "gp2"
    delete_on_termination = true
  }  
}

resource "aws_autoscaling_group" "nodes" {
  name                 = "ASG-${aws_launch_configuration.nodes.name}"
  launch_configuration = aws_launch_configuration.nodes.name
  min_size             = var.clusternodecount
  max_size             = var.clusternodecount
  vpc_zone_identifier  = [aws_default_subnet.default_az1.id, aws_default_subnet.default_az2.id, aws_default_subnet.default_az3.id]
  load_balancers       = [aws_elb.lb.name]
  
  tag {
      key                 = "Name"
      value               = "Nodes in ASG from Terraform"
      propagate_at_launch = true
    }
      
  lifecycle {
    create_before_destroy = false
  }
}

#lb

resource "aws_elb" "lb" {
  name               = "HHdemoapp"
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1], data.aws_availability_zones.available.names[2]]
  security_groups    = [aws_security_group.Worldports.id]
  
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = var.nodeport
    instance_protocol = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:${var.nodeport}/"
    interval            = 5
  }
  tags = {
    Name = "HHdemoapp"
  }
}


#net

resource "aws_default_subnet" "default_az1" {
  availability_zone = data.aws_availability_zones.available.names[0]
 # map_public_ip_on_launch = false - возможно, правильней было бы и вовсе выкрутить все ноды из интернета, но тогда для обслуживания этого проекта
 # будет нужно за NAT-gateway платить, чего мне пока не хочется
  }

resource "aws_default_subnet" "default_az2" {
  availability_zone = data.aws_availability_zones.available.names[1]
  }

resource "aws_default_subnet" "default_az3" {
  availability_zone = data.aws_availability_zones.available.names[2]
 }

# в теории, терраформ мог бы и сам позвать ansible, без гитлаба, 
# но мне кажется это будет не по феншую, так что появилась другая идея:


data "aws_instances" "cluster_instances_meta" {
  instance_tags = {
    Name = "Nodes in ASG from Terraform"
  }
  depends_on = [aws_autoscaling_group.nodes]
}

output "privateips" {
    value = data.aws_instances.cluster_instances_meta.private_ips
}
