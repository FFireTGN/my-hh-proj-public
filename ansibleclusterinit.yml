- hosts: all
  become: yes
  gather_facts: no
  tasks:  

    - name: Wait for system to become reachable
      wait_for_connection:

    - name: Swap off
      shell: swapoff -a
  
    - name: Remove Swap from fstab
      mount:
        name: swap
        fstype: swap
        state: absent

    - name: disable firewall
      ufw: 
        state: disabled

    - name: install HTTPS
      apt:
        name: apt-transport-https
        state: present
 
  # в качестве CRI решил попробовать использовать containerd без docker, поэтому получилось всё чуть сложнее, чем обычно

    - name: overlay
      modprobe:
        name: overlay
        state: present

    - name: br_netfilter
      modprobe:
        name: br_netfilter
        state: present
    
    - name: ip_forward
      shell: echo '1' > /proc/sys/net/ipv4/ip_forward

    - name: Add Docker GPG apt Key
      apt_key:
        url: https://download.docker.com/linux/ubuntu/gpg
        state: present

    - name: Add Docker Repository (containerd setup requires)
      apt_repository:
        repo: deb https://download.docker.com/linux/ubuntu focal stable
        state: present
  
    - name: Install CRI
      apt:
       name: containerd.io
       state: present
       update_cache: true

    - name: create containerd directory
      file:
        path: /etc/containerd
        state: directory
        mode: 0755
        owner: ubuntu

    - name: Configure containerd
      shell: containerd config default | tee /etc/containerd/config.toml

    - name: Using the systemd cgroup driver
      lineinfile:
        path: /etc/containerd/config.toml
        insertafter: '[plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]'
        line: 'SystemdCgroup = true'

    - name: containerd reload 
      systemd:
        state: restarted
        daemon_reload: yes
        name: containerd

    - name: add Kubernetes apt-key
      apt_key:
        url: https://packages.cloud.google.com/apt/doc/apt-key.gpg
        state: present

    - name: add Kubernetes' APT repository
      apt_repository:
        repo: deb http://apt.kubernetes.io/ kubernetes-xenial main
        state: present
        filename: 'kubernetes'

    - name: install kubelet
      apt:
        name: kubelet
        state: present
        update_cache: true

    - name: install kubeadm
      apt:
        name: kubeadm
        state: present

- hosts: master
  become: yes
  gather_facts: false
  tasks:


    - name: install kubectl
      apt:
        name: kubectl
        state: present

    - name: Register File Stat
      stat:
        path: /etc/kubernetes/admin.conf
      register: stat_result
         
    - name: cluster init
      shell: kubeadm init --pod-network-cidr=10.244.0.0/16
      when: stat_result.stat.exists == False 


    - name: kube directory
      file:
        path: $HOME/.kube
        state: directory
        mode: 0600
        owner: ubuntu
      
    - name: kube directory for ci/cd process
      file:
        path: /home/gitlab-runner/.kube/
        state: directory
        mode: 0700
        owner: gitlab-runner
      delegate_to: localhost
     
 
    - name: copy admin.conf for control plane node
      copy:
        src: /etc/kubernetes/admin.conf
        dest: $HOME/.kube/config
        remote_src: yes
        owner: ubuntu
     
    - name: copy admin.conf to local runner for ci/cd process
      fetch:
        src: /etc/kubernetes/admin.conf
        dest: /home/gitlab-runner/.kube/config
        remote_src: yes
        flat: yes

    - name: install weave CNI
      shell: kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

  #  изначально я пытался использовать flannel как cni, но в итоге его пришлось выкомментировать в пользу weave, т.к. даже без инкапсуляции vx-lan
  #  у меня не получилось добится стабильной работы ингресса (в минимальной комплектации при этом istio поднимался без проблем).
  #  Можно раскомментировать этот блок для замены weave на flannel:
  #
  #  - name: Download CNI
  #    get_url:
  #      url: https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml
  #      dest: /usr/kube-flannel.yml

  #  - name: CNI switch vxlan to host-gw
  #    lineinfile:
  #      path: /usr/kube-flannel.yml
  #      search_string: 'vxlan'
  #      line: 'host-gw'

  #  - name: install CNI
  #    shell: kubectl apply -f '/usr/kube-flannel.yml'

    - name: get join command
      shell: kubeadm token create --print-join-command
      register: join_command_raw
    
    - name: set join command
      set_fact:
        join_command: "{{ join_command_raw.stdout_lines[0] }}"

# кластер маленький и демонстрационный, из соображений экономии я разрешаю планировщику планировать на мастер-ноду в том числе 
# (в реальности не надо так делать):

    - name: cp sheduling enable
      shell: kubectl taint nodes --all node-role.kubernetes.io/master-
      ignore_errors: yes
    
# это гипотетически пользователь с ограниченными правами (RBAC применяется в ci/cd далее),
# по задумке он мог бы управлять непосредственно развертываниями в кластере,
# создается только при создании кластера, в целом необязательная опциональность:

    - name: usercreate
      shell: openssl genrsa -out deployment_manager 2048 && \
             openssl req -new -key deployment_manager -out deployment_manager.csr -subj "/CN=deployment_manager/O=deparment" && \
             openssl x509 -req -in deployment_manager.csr -CA /etc/kubernetes/pki/ca.crt -CAkey /etc/kubernetes/pki/ca.key -CAcreateserial -out deployment_manager.crt -days 365 && \
             mkdir ~/.kube/certs && \
             cp *.* ~/.kube/certs && \
             kubectl config set-credentials deployment_manager --client-certificate=$HOME/.kube/certs/deployment_manager.crt --client-key=$HOME/.kube/certs/deployment_manager.key && \
             kubectl config set-context deployment_manager --cluster=kubernetes --namespace=default --user=deployment_manager
      when: stat_result.stat.exists == False
      
- hosts: workers
  become: yes
  gather_facts: false
  tasks:
           
    - name: join cluster
      shell: '{{hostvars[groups["master"][0]].join_command}}'
     # when: hostvars[groups["master"][0]].stat_result.stat.exists == False это условие требовалось для предыдущей версии плейбука
     # игнорировать ошибки на этом шаге, чтобы отвалившаяся нода могла присоеденится, пока работающая ругается на то что она уже присоеденена
      ignore_errors: yes